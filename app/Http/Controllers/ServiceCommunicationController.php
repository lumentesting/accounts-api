<?php

namespace App\Http\Controllers;

use App\Models\ApiKeysModel;
use Illuminate\Http\Request;

class ServiceCommunicationController extends Controller
{
    public function __construct()
    {
        //
    }


    public static function checkServiceApi(Request $request)
    {
        $response = ApiKeysModel::checkApiKeyForServiceCommunication($request);

        if ($response == false) {
            return responder()->error(406, "Request from unknown external API.")->respond(406);
        } else {
            return responder()->success();
        }
    }

}
