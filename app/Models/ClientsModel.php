<?php

namespace App\Models;

use App\Helpers\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Helper\Helper;

class ClientsModel extends Model
{
    protected $table = 'clients';

    protected $fillable = [
        'email', 'password', 'token'
    ];


    //Client registration
    public static function clientRegistration( $params )
    {
        if (empty($params['email']) || empty($params['password']) || empty($params['repeatPassword'])) {
            return responder()->error(406, "Fields shouldn't be empty")->respond(406);
        }

        $checkIfClientExists = DB::table('clients')->where('email', $params['email'])->first();

        if (Empty($checkIfClientExists)) {

            if (strlen($params['password']) < 8) {
                return responder()->error(406, "Password should be at least 8 characters long")->respond(406);
            }

            if ($params['password'] != $params['repeatPassword']) {
                return responder()->error(406, "Passwords don't match")->respond(406);
            }

            $params['password'] = app('hash')->make($params['password']);
            $params['token']    = str_random(60);

            ClientsModel::create($params->all());

            $log = [
                'description' => 'User ' . $params['email'] . ' has been registered successfully'
            ];
            Helpers::Log($log);

            $data = [
                'message' => "User has been successfully registered"
            ];
            return responder()->success($data);
        }
        else {
            return responder()->error(406, "Email is already taken")->respond(406);
        }
    }


    //Client login
    public static function clientLogin( $params )
    {
        $findClient = DB::table('clients')->where('email', $params['email'])->first();

        if (!Empty($findClient)) {

            if (Hash::check($params['password'], $findClient->password)) {

                $id    = $findClient->id;
                $token = str_random(60);
                ClientsModel::where('email', $findClient->email)->update(['token' => $token]);


                $log = [
                    'description' => 'User ' . $params['email'] . ' logged in successfully'
                ];
                Helpers::Log($log);


                $data = [
                    'message' => "You are now logged in.",
                    'id'      => $id,
                    'token'   => $token
                ];

                return responder()->success($data);
            }
            else {
                //Logging
                $log = [
                    'description' => 'User ' . $params['email'] . ' Tried to log - in. Error: "Password is incorrect"'
                ];
                Helpers::Log($log);

                return responder()->error(406, "Password is incorrect")->respond(406);
            }
        }
        else {
            //Logging
            $log = [
                'description' => 'User tried to log - in. Error: "User doesnt exist"'
            ];
            Helpers::Log($log);

            return responder()->error(406, "User doesn't exist")->respond(406);
        }
    }


    //Check client token
    public static function checkClientToken( $params )
    {
        $findToken = DB::table('clients')->where('token', $params->header('token'))->first();

        if (Empty($findToken)) {
            return false;
        }
        else {
            return true;
        }
    }

}
