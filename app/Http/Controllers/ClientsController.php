<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\ApiKeysModel;
use App\Models\ClientsModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{

    public function __construct()
    {
        //
    }


    //Register client
    public function registerClient(Request $request)
    {
        return ClientsModel::clientRegistration($request);
    }


    //Login client
    public function loginClient(Request $request)
    {
        return ClientsModel::clientLogin($request);
    }


    //Check token for other services
    public function checkClientToken(Request $request)
    {
        $clientApiResult = ApiKeysModel::checkApiKeyForServiceCommunication($request);
        $tokenResult = ClientsModel::checkClientToken($request);

        if ($clientApiResult == false) {
            return responder()->error(406,"Request from unknown external API.")->respond(406);
        }

        if ($tokenResult == false) {
            return responder()->error(406,"Access is forbidden for unauthorized users.")->respond(406);
        }

        return responder()->success();
    }


    //Check token
    public function checkToken(Request $request)
    {
        $tokenResult = ClientsModel::checkClientToken($request);


        if ($tokenResult == false) {
            return responder()->error(406,"Access is forbidden for unauthorized users.")->respond(406);
        }

        return responder()->success();
    }
}
