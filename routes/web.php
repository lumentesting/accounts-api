<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return $router->app->version();
});

//, 'middleware' => 'serviceAccessMiddleware'
$router->group(['prefix' => 'clients', 'middleware' => 'serviceAccessMiddleware'], function($router){
    $router->post('register', 'ClientsController@registerClient');
    $router->post('login', 'ClientsController@loginClient');
    $router->post('checkClientToken', 'ClientsController@checkClientToken');
    $router->post('checkToken', 'ClientsController@checkToken');
});

$router->group(['prefix' => 'services', 'middleware' => 'serviceAccessMiddleware'], function($router){
    $router->post('checkServiceApi', 'ServiceCommunicationController@checkServiceApi');
});


