<?php


namespace App\Http\Middleware;

use App\Models\ApiKeysModel;
use Closure;

class ServiceAccessMiddleware
{
    public function handle($request, Closure $next)
    {
        $apiKeyResult = ApiKeysModel::checkApiKey($request);

        if ($apiKeyResult == false) {
            return responder()->error(406, "Request from unknown API.")->respond(406);
        }
        return $next($request);
    }
}